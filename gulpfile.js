// gulpfile.js
// Luis Matute
// May-16

"use strict";

// Dependencies =================================
    var gulp = require('gulp');
    var webserver = require('gulp-webserver');

// Webserver Task ===============================
    gulp.task('webserver', function() {
        gulp.src('./')
            .pipe(webserver({
                port: 3001,
                livereload: true,
                directoryListing: true,
                fallback: 'index.html'
        }));
    });

// Default Task =================================
    gulp.task('default',['webserver']);